// import { Directive, HostListener, HostBinding, OnInit } from '@angular/core'

// @Directive({
//     selector: '[appDropdown]'
// })
// export class DropdownDirective implements OnInit {
//     @HostBinding('class.open') isOpen = false;
//     @HostListener('click') toggleOpen() {
//         this.isOpen = !this.isOpen
//     }
//     ngOnInit(){

//     }
// }

import { Directive, HostListener, HostBinding, OnInit, ElementRef } from '@angular/core'

@Directive({
    selector: '[appDropdown]'
})
export class DropdownDirective implements OnInit {
    @HostBinding('class.open') isOpen = false;
    @HostListener('document:click', ['$event']) toggleOpen(event: Event) {
        this.isOpen = this.elfRef.nativeElement.contains(event.target) ? !this.isOpen : false;
    }
    constructor(
        private elfRef: ElementRef
    ) {

    }
    ngOnInit() {

    }
}