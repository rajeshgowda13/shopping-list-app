import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Ingrediant } from '../shared/ingrediant.model';
import { ShoppingListService } from './shopping-list.service';
import { Subscription } from 'rxjs';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit, OnDestroy {
  ingrediants: Ingrediant[];
  private igChangeSub: Subscription;
  constructor(
    private shoppingListService: ShoppingListService
  ) { }

  ngOnInit() {
    this.ingrediants = this.shoppingListService.getIngrediants();
    this.igChangeSub = this.shoppingListService.ingrediantChanged.subscribe(
      (ingrediants: Ingrediant[]) => {
        this.ingrediants = ingrediants;
      }
    )
  }
  ngOnDestroy() {
    this.igChangeSub.unsubscribe()
  }
  onEditItem(index:  number){
    this.shoppingListService.startedEditing.next(index);

  }
}
