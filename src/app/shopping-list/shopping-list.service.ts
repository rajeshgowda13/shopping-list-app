import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { Ingrediant } from '../shared/ingrediant.model';

@Injectable({
  providedIn: 'root'
})
export class ShoppingListService {
  ingrediantChanged = new Subject<Ingrediant[]>();
  startedEditing = new Subject<number>();
  private ingrediants: Ingrediant[] = [
    new Ingrediant('Apples', 23),
    new Ingrediant('Orange', 12)
  ]
  getIngrediants() {
    return this.ingrediants.slice();
  }
  getIngrediant(index: number){
    return this.ingrediants[index]

  }
  addIngrediant(ingrediants: Ingrediant) {
    this.ingrediants.push(ingrediants);
    this.ingrediantChanged.next(this.ingrediants.slice())
  }
  addIngrediants(ingrediants: Ingrediant[]) {
    this.ingrediants.push(...ingrediants);
    this.ingrediantChanged.next(this.ingrediants.slice())
  }
  updateIngrediant(index: number, newIngrediant: Ingrediant){
    this.ingrediants[index] = newIngrediant;
    this.ingrediantChanged.next(this.ingrediants.slice());
  }
  deleteIngrediant(index: number){
    this.ingrediants.splice(index,1);
    this.ingrediantChanged.next(this.ingrediants.slice())

  }
  constructor() { }
}
