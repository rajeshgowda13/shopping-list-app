import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { Ingrediant } from 'src/app/shared/ingrediant.model';
import { ShoppingListService } from '../shopping-list.service';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {
  @ViewChild('f', {static: false}) slForm: NgForm;
  subscription: Subscription;
  editMode = false;
  editedItemIndex: number;
  editedItem: Ingrediant
  //   @ViewChild('nameInput', { static: false }) nameInputRef: ElementRef;
  //   @ViewChild('amountInput', { static: false }) amountInputRef: ElementRef;
  // @Output() ingrediantAdded = new EventEmitter<Ingrediant>();



  constructor(
    private slService: ShoppingListService
  ) { }

  ngOnInit() {
    this.subscription = this.slService.startedEditing.subscribe(
      (index: number)=>{
        this.editMode = true;
        this.editedItemIndex = index;
        this.editedItem = this.slService.getIngrediant(index);
        this.slForm.setValue({
          name: this.editedItem.name,
          amount: this.editedItem.amount
        })
      }
    );
  }
  onSubmit(form: NgForm) {
    // const ingName = this.nameInputRef.nativeElement.value;
    // const ingAmount = this.amountInputRef.nativeElement.value;
    const value = form.value;
    const newIngrediant = new Ingrediant(value.name, value.amount)
    // this.slService.addIngrediant(newIngrediant);
    if(this.editMode){
      this.slService.updateIngrediant(this.editedItemIndex, newIngrediant)
    } else{
      this.slService.addIngrediant(newIngrediant);
    }
    this.editMode = false;
    form.reset()
    // this.ingrediantAdded.emit(newIngrediant)
  }
  onClear(){
    this.slForm.reset();
    this.editMode = false;
  }
  onDelete(){
    this.slService.deleteIngrediant(this.editedItemIndex);
    this.onClear();
  }
  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}
